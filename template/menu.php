<div class="container">
    <nav class="navbar navbar-transparent hidden-xs">
	<div class="container-fluid">
	    <div class="navbar">
		<ul class="nav navbar-nav">
		    <li><a href="strona-glowna">
			<div class="inner">Strona Główna</div> 
		    </a></li>
		    <li><a href="o-stronie">
			<div class="inner">O stronie</div>
		    </a></li>
		    <?php if(isset($_SESSION['user'])){
          echo '<li><a href="wyloguj">
			<div class="inner">Witam '.$_SESSION['user']."!(wyloguj)</div>
		    </a></li>";
                    }
                    else{?><li><a href="logowanie">
			<div class="inner">Logowanie</div>
                    </a></li><?php }?>
		</ul>
	    </div>
	</div>
    </nav>
    
   <nav class="navbar navbar-default navbar-mobile hidden-sm hidden-md hidden-lg">
	<div class="container-fluid">
	    
	    <div class="navbar">
		<ul class="nav navbar-nav">
		    <li><a href="strona-glowna">Strona Główna</a></li>
		    
		    <li><a href="o-stronie">O stronie</a></li>
		    
		    <?php if(isset($_SESSION['user'])){
          echo '<li><a href="wyloguj">Witam '.$_SESSION['user']."!(wyloguj)</a></li>";
                    }else{?><li><a href="logowanie">Logowanie</a></li><?php }?>
		</ul>
	    </div>
	</div>
    </nav>
</div>