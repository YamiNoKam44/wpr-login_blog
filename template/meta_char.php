<?php require_once 'config.php';?>
<title><?php echo $site_name; ?></title>
        <meta name="keywords" content="blog , pjatk, s13460 , projekt, warsztaty programistyczne">
        <meta name="description" content="Projekt blogu z modelem logowania, kasowania itd. by s13460">
        
        <!--Open Graph Protocol-->
        <meta property="og:title" content="Projekt Blog s13460, Blog">
        <meta property="og:type" content="blog">
        <meta property="og:url" content="http://www.<?php echo $domain; ?>/">
        <meta property="og:image" content="http://www.<?php echo $domain; ?>/images/">
        <meta property="og:site_name" content="<?php echo $site_name; ?>">
        <meta property="og:description" content="Projekt Blog s13460, Blog">    

        <!--Dublin Core-->
        <meta name="DC.Title" content="Projekt Blog s13460, Blog">
        <meta name="DC.Creator" content="YamiNoKam44[s13460]">
        <meta name="DC.Subject" content="Blog">
        <meta name="DC.Description" content="Projekt Blog s13460, Blog">
        <meta name="DC.Publisher" content="YamiNoKam44[s13460]">
        <meta name="DC.Type" content="text">
        <meta name="DC.Format" content="text/html">
        <meta name="DC.Language" content="pl">
        <meta name="DC.Rights" content="YamiNoKam44[s13460]">        

        <link rel="alternate" href="http://www.<?php echo $domain; ?>" hreflang="pl-pl" />        
        